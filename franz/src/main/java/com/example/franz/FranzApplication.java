package com.example.franz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FranzApplication {

	public static void main(String[] args) {
		SpringApplication.run(FranzApplication.class, args);
	}

}
