package com.example.franz.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TestController
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@RestController
public class TestController {

    @Value("${pozdrav}")
    private String pozdrav;

    @GetMapping
    public String franzovaOdpoved() {
        return pozdrav;
    }
}
