package com.example.hitman.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * FranzClient
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@FeignClient("franz")
public interface FranzClient {
    @GetMapping
    String franzovaOdpoved();
}
