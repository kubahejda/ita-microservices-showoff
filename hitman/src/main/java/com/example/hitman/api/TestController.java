package com.example.hitman.api;

import com.example.hitman.client.FranzClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TestController
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@RestController
@RequiredArgsConstructor
public class TestController {

    private final FranzClient client;

    @GetMapping
    public String pozdravFranze() {
        return "Hitman zdravil Franze a ten odpovedel:" + client.franzovaOdpoved();
    }
}
